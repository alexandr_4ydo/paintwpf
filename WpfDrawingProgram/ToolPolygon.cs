﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WpfDrawingProgram
{
    class ToolPolygon : ToolLine
    {
        List<Point> PointList;
        public ToolPolygon(double x, double y)
            : base(x, y)
        
        {
            PointList = new List<Point>();
            PointList.Add(new Point(x,y));
        }

        public override void MakeStepTo(double x, double y)
        {
            PointList.Add(new Point(x, y));
            DoDraw();
        }

        protected override void Draw(DrawingContext dc)
        {
            for (int i = 1; i < PointList.Count; ++i)
            {
                dc.DrawLine(new Pen(new SolidColorBrush(color1), width), PointList[i - 1], PointList[i]);
            }
            dc.DrawLine(new Pen(new SolidColorBrush(color1), width), PointList[PointList.Count - 1], new Point(x2,y2));
        }

        public override void MoveTo(double deltaX, double deltaY)
        {
            for (int i = 0; i < PointList.Count; ++i)
            {
                Point p = PointList[i];
                p.X += deltaX;
                p.Y += deltaY;
                PointList[i] = p;
            }
            x2 += deltaX;
            y2 += deltaY;
        }
    }
}
