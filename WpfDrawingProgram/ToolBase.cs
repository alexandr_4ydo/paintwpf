﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WpfDrawingProgram
{
    public class ToolBase: DrawingVisual
    {
        public double x1 { get; set; }
        public double x2 { get; set; }
        public double y1 { get; set; }
        public double y2 { get; set; }

        private bool selected = false;
        public bool isSelected { 
            get 
            {
                return selected; 
            }
            set
            {
                selected = value;
                DoDraw();
            }
        }

        public Color color1 { get; set; }
        public Color color2 { get; set; }
        public double width { get; set; }

        private DrawingMode drawingMode = DrawingMode.Wire;
        public DrawingMode mode {
            get
            {
                return drawingMode;
            }
            set
            {
                drawingMode = value;
                SelectBrush();
            }
        }

        protected Brush brush;

        public ToolBase(double x, double y)
        {
            this.x1 = x;
            this.y1 = y;
            this.x2 = this.x1 + 1;
            this.y2 = this.y1 + 1;           
        }

        public virtual void MoveTipTo(double x, double y)
        {
            this.x2 = x;
            this.y2 = y;
            DoDraw();
        }

        public virtual void MoveTo(double deltaX, double deltaY)
        {
            this.x1 += deltaX;
            this.y1 += deltaY;
            this.x2 += deltaX;
            this.y2 += deltaY;
            DoDraw();
        }

        public bool IsHit(Point point)
        {
            return ContentBounds.Contains(point);
        }

        protected virtual void Draw(DrawingContext context) { }

        protected void DoDraw()
        {
            DrawingContext context = this.RenderOpen();

            if (isSelected)
            {
                DrawBorder(context);
            }

            Draw(context);
            context.Close();
        }

        private void DrawBorder(DrawingContext context)
        {
            context.DrawRectangle(null, new Pen(new SolidColorBrush(Colors.LightSkyBlue),3.0), ContentBounds);          
        }

        private void SelectBrush()
        {
            switch (mode)
            {
                case DrawingMode.Wire: brush = null; break;
                case DrawingMode.Filled: brush = new SolidColorBrush(color2); break;
                case DrawingMode.Gradient: brush = new LinearGradientBrush(color1, color2, 90.0); break;
            }
        }
        public virtual void MakeStepTo(double x, double y)
        {

        }
    }
}
