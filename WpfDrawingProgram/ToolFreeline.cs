﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WpfDrawingProgram
{
    class ToolFreeline : ToolBase
    {
        protected List<Point> PointList;

        public ToolFreeline(double x, double y)
            : base(x, y)
        {
            PointList = new List<Point>();
        }

        protected override void Draw(DrawingContext dc)
        {
            for (int i = 1; i < PointList.Count; ++i)
            {
                dc.DrawLine(new Pen(new SolidColorBrush(color1), width), PointList[i - 1], PointList[i]);
            }
        }

        public override void MoveTipTo(double x, double y)
        {
            this.x2 = x;
            this.y2 = y;
            PointList.Add(new Point(x, y));
            DoDraw();
        }

        public override void MoveTo(double deltaX, double deltaY)
        {
            for (int i = 0; i < PointList.Count; ++i)
            {
                Point p = PointList[i];
                p.X += deltaX;
                p.Y += deltaY;
                PointList[i] = p;
            }
        }
    }
}
