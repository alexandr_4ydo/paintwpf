﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfDrawingProgram
{
    public enum DrawingMode {Wire, Filled, Gradient}
    /// <summary>
    /// Логика взаимодействия для ControlWindow.xaml
    /// </summary>
    public partial class ControlWindow : Window
    {
        public delegate void DrawMode(DrawingMode mode, Color FillColor, Color color);
        public delegate void SetWidth(int width);
        public delegate void ShapeSelection(WpfDrawingProgram.Figure shape);
        public delegate void FillSurface(Brush brush);
        public delegate void RemoveFigure();
        public delegate void CopyToClipboard();
        
        public event ShapeSelection OnShapeSelected = delegate {};
        public event SetWidth OnSetWidth = delegate {};
        public event FillSurface OnFillSurface = delegate { };
        public event RemoveFigure OnRemove = delegate { };
        public event CopyToClipboard OnCopy = delegate { };
        public event DrawMode OnDrawMode = delegate { };

        DrawingMode mode;
        Color color1 = Colors.Black;
        Color color2 = Colors.Red;
        bool gradientColor = false;

        bool colorFeedback = true;

        public ControlWindow()
        {
            InitializeComponent();
            ColorView1.Background = new SolidColorBrush(color1);
            ColorView2.Background = new SolidColorBrush(color2);
            mode = new DrawingMode();
        }

        private void RadioButton_Checked_Freeline(object sender, RoutedEventArgs e)
        {
            OnShapeSelected(WpfDrawingProgram.Figure.FreeLine); 
        }

        private void RadioButton_Checked_Line(object sender, RoutedEventArgs e)
        {
            OnShapeSelected(WpfDrawingProgram.Figure.Line); 
        }

        private void Rectangle_Checked(object sender, RoutedEventArgs e)
        {
            OnShapeSelected(WpfDrawingProgram.Figure.Rectangle); 
        }

        private void Ellipse_Checked(object sender, RoutedEventArgs e)
        {
            OnShapeSelected(WpfDrawingProgram.Figure.Ellipse); 
        }

        private void Polygon_Checked(object sender, RoutedEventArgs e)
        {
            OnShapeSelected(WpfDrawingProgram.Figure.Polygon); 
        }
        private void Circle_Checked(object sender, RoutedEventArgs e)
        {
            OnShapeSelected(WpfDrawingProgram.Figure.Circle); 
        }

        private void Pointer_Checked(object sender, RoutedEventArgs e)
        {
            OnShapeSelected(WpfDrawingProgram.Figure.Pointer); 
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (e.NewValue >= 1)
                OnSetWidth((int)e.NewValue);
            else
                OnSetWidth(1);
        }

        private void SliderColor_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (colorFeedback)
            {
                if (gradientColor)
                {
                    color2.R = (byte)SliderRed.Value;
                    color2.G = (byte)SliderGreen.Value;
                    color2.B = (byte)SliderBlue.Value;
                    color2.A = (byte)SliderA.Value;

                }
                else
                {
                    color1.R = (byte)SliderRed.Value;
                    color1.G = (byte)SliderGreen.Value;
                    color1.B = (byte)SliderBlue.Value;
                    color1.A = (byte)SliderA.Value;

                }
            }

            if (ColorView2 != null)
                ColorView2.Background = new SolidColorBrush(color2);

            if (ColorView1 != null)
                ColorView1.Background = new SolidColorBrush(color1);
            
            OnDrawMode(mode, color2, color1);
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(mode == DrawingMode.Gradient)
            {
                OnFillSurface(new LinearGradientBrush(color1, color2, 90.0));
            }
            else if(mode == DrawingMode.Filled)
            {
                OnFillSurface(new SolidColorBrush(color2));
            }
            else if(mode == DrawingMode.Wire)
            {
                OnFillSurface(new SolidColorBrush(Colors.White));
            }
       }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            OnRemove();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            OnCopy();
            MessageBox.Show("The drawing has been copied to the clipboard!");
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            gradientColor = true;
            colorFeedback = false;
            SliderA.Value = color2.A;
            SliderRed.Value = color2.R;
            SliderGreen.Value = color2.G;
            SliderBlue.Value = color2.B;
            colorFeedback = true;
        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            gradientColor = false;
            colorFeedback = false;
            SliderA.Value = color1.A;
            SliderRed.Value = color1.R;
            SliderGreen.Value = color1.G;
            SliderBlue.Value = color1.B;
            colorFeedback = true;
        }
        private void Wire_Checked(object sender, RoutedEventArgs e)
        {
            mode = DrawingMode.Wire;
            OnDrawMode(mode, color2, color1);
        }

        private void Filled_Checked(object sender, RoutedEventArgs e)
        {
            mode = DrawingMode.Filled;
            OnDrawMode(mode, color2, color1);
        }

        private void Gradient_Checked(object sender, RoutedEventArgs e)
        {
            mode = DrawingMode.Gradient;
            OnDrawMode(mode, color2, color1);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start("Help.docx");
            }
            catch (Exception ex) { }
        }

    }
}
