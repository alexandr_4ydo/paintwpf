﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;

namespace WpfDrawingProgram
{
    public enum Figure
    {
        FreeLine,
        Line,
        Rectangle,
        Ellipse,
        Polygon,
        Circle,
        Pointer,
    }

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ControlWindow controlWindow;
        public MainWindow()
        {
            InitializeComponent();
            CreateControlWindow();                  
        }

        void CreateControlWindow()
        {
            controlWindow = new ControlWindow();
            controlWindow.OnShapeSelected += m_canvas.OnShapeSelected;
            controlWindow.OnSetWidth += m_canvas.OnSetWidth;
            controlWindow.OnFillSurface += m_canvas.OnFillSurface;
            controlWindow.OnRemove += m_canvas.OnRemoveFigure;
            controlWindow.OnCopy += m_canvas.OnCopyToClipboard;
            controlWindow.OnDrawMode += m_canvas.OnSetColor;
            controlWindow.Closed += Window_Closed;
            controlWindow.Show();

            Closed += Window_Closed;
         }

        void Window_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}
