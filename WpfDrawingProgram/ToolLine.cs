﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WpfDrawingProgram
{
    public class ToolLine : ToolBase
    {
        public ToolLine(double x, double y)
            : base(x, y)
        {
        }

        protected override void Draw(DrawingContext dc)
        {
            dc.DrawLine(new Pen(new SolidColorBrush(color1), width), new Point(x1, y1), new Point(x2, y2));
        }
    }
}
