﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WpfDrawingProgram
{
    class ToolCircle : ToolEllipse
    {
        public ToolCircle(double x, double y)
            : base(x, y)
        {
        }
        protected override void Draw(DrawingContext dc)
        {
            Point center = new Point((x1 + x2) / 2.0, (y1 + y2) / 2.0);

            double radiusX = (x1 - x2) / 2.0;
            double radiusY = (y1 - y2) / 2.0;

            double radius = Math.Min(radiusX, radiusY);
            double delta = Math.Min(x2 - x1, y2 - y1);

            if (delta != 0)
            {
                x2 = x1 + delta;
                y2 = y1 + delta;

                Point p1 = this.PointToScreen(new Point(x1, y1));
                Point p2 = this.PointToScreen(new Point(x2, y2));
                int left = (int)Math.Min(p1.X, p2.X);
                int right = (int)Math.Max(p1.X, p2.X);
                int top = (int)Math.Min(p1.Y, p2.Y);
                int bottom = (int)Math.Max(p1.Y, p2.Y);

                System.Drawing.Rectangle clip = new System.Drawing.Rectangle(left, right, top, bottom);
                CustomCanvas.ClipCursor(ref clip);
            }

            dc.DrawEllipse(brush, new Pen(new SolidColorBrush(color1), width), center, radius, radius);
        }
    }
}
