﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace WpfDrawingProgram
{
    public class CustomCanvas: Canvas
    {
        VisualCollection m_graphicsList;
        ToolBase m_lastTool, m_selectedTool;
        Figure m_figure = new Figure();
        Point m_startDrawingPt = new Point();
        Point m_endDrawingPt = new Point();
        Point m_lastPt = new Point();
        bool m_mouseDown = false;
        int m_width = 1;
        Color m_mainColor = Colors.Black;
        Color m_secondColor = new Color();
        DrawingMode m_Mode;

        public Color Colour
        {
            get { return m_mainColor; }
        }

        public CustomCanvas() : base()
        {
            m_graphicsList = new VisualCollection(this);

            this.Visibility = Visibility.Visible;

            this.FocusVisualStyle = null;

            this.MouseLeftButtonDown += Grid_MouseDown;
            this.MouseLeftButtonUp += Grid_MouseUp;
            this.MouseMove += Grid_MouseMove;
            this.MouseRightButtonDown += CustomCanvas_MouseRightButtonDown;
        }


        public void OnFillSurface(Brush brush)
        {
            this.Background = brush;
        }

        public void OnSetColor(DrawingMode Mode, Color FillColor, Color color)
        {
            m_secondColor = FillColor;
            m_mainColor = color;
            m_Mode = Mode;
        }

        public void OnShapeSelected(Figure figure)
        {
            m_figure = figure;
        }

        public void OnRemoveFigure()
        {
            if(m_selectedTool != null)
            {
                m_graphicsList.Remove(m_selectedTool);
                m_selectedTool = null;
            }
        }

        public void OnCopyToClipboard()       
        {
            RenderTargetBitmap bmpCopied = new RenderTargetBitmap((int)Math.Round(this.ActualWidth), (int)Math.Round(this.ActualHeight), 96, 96, PixelFormats.Default);
            bmpCopied.Render(this);
            Clipboard.SetImage(bmpCopied);
        }

        public void OnSetWidth(int width)
        {
            m_width = width;
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            m_mouseDown = true;
            m_startDrawingPt = e.GetPosition(this);
            m_lastPt = e.GetPosition(this);

            switch (m_figure)
            {
                case Figure.Line:
                        m_lastTool = new ToolLine(m_startDrawingPt.X, m_startDrawingPt.Y); break;
                case Figure.FreeLine:
                        m_lastTool = new ToolFreeline(m_startDrawingPt.X, m_startDrawingPt.Y); break;
                case Figure.Ellipse:
                        m_lastTool = new ToolEllipse(m_startDrawingPt.X, m_startDrawingPt.Y); break;
                case Figure.Circle:
                        m_lastTool = new ToolCircle(m_startDrawingPt.X, m_startDrawingPt.Y); break;
                case Figure.Pointer:
                    {
                        if (m_selectedTool != null)
                        {
                            m_selectedTool.isSelected = false;
                        }
                        SelectFigure(new Point(m_startDrawingPt.X, m_startDrawingPt.Y));
                        return;
                    }
                case Figure.Polygon:
                        m_lastTool = new ToolPolygon(m_startDrawingPt.X, m_startDrawingPt.Y); break;
                case Figure.Rectangle:
                        m_lastTool = new ToolRectangle(m_startDrawingPt.X, m_startDrawingPt.Y); break;
            }

            if (m_lastTool != null)
            {
                m_lastTool.color1 = m_mainColor;
                m_lastTool.width = m_width;
                m_lastTool.color2 = m_secondColor;
                m_lastTool.mode = m_Mode;
                m_graphicsList.Add(m_lastTool);
            }
        }

        private void Grid_MouseMove(object sender, MouseEventArgs e)
        {
            m_endDrawingPt = e.GetPosition(this);

            if (m_mouseDown && m_lastTool != null)
            {
                m_lastTool.MoveTipTo(m_endDrawingPt.X, m_endDrawingPt.Y);
            }
            else if (m_mouseDown && m_selectedTool != null)
            {
                double dx = m_endDrawingPt.X - m_lastPt.X;
                double dy = m_endDrawingPt.Y - m_lastPt.Y;

                m_lastPt = m_endDrawingPt;

                m_selectedTool.isSelected = false;
                m_selectedTool.MoveTo(dx, dy);
            }
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            ClipCursor(IntPtr.Zero);
            m_mouseDown = false;        
            m_lastTool = null;
        }

        void CustomCanvas_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            Point p = e.GetPosition(this);
            if(m_lastTool!=null)
                m_lastTool.MakeStepTo(p.X, p.Y);
        }

        protected override int VisualChildrenCount
        {
            get { return m_graphicsList.Count; }
        }

        protected override Visual GetVisualChild(int index)
        {
            return m_graphicsList[index];
        }
        
        void SelectFigure(Point point)
        {
            foreach(ToolBase p in m_graphicsList)
            {
                Rect rect = p.ContentBounds;
                if(p.IsHit(point))
                {
                    p.isSelected = true;
                    m_selectedTool = p;
                    break;
                }
            }
        }

        [DllImport("user32.dll")]
        public static extern void ClipCursor(ref System.Drawing.Rectangle rect);

        [DllImport("user32.dll")]
        public static extern void ClipCursor(IntPtr rect);

    }


}
