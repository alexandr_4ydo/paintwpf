﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WpfDrawingProgram
{
    class ToolEllipse : ToolBase
    {
        public ToolEllipse(double x, double y)
            : base(x, y)
        {
        }

        protected override void Draw(DrawingContext dc)
        {
            Point center = new Point((x1 + x2) / 2.0, (y1 + y2) / 2.0);

            double radiusX = (x1 - x2) / 2.0;
            double radiusY = (y1 - y2) / 2.0;

            dc.DrawEllipse(brush, new Pen(new SolidColorBrush(color1), width), center, radiusX, radiusY);
        }
    }
}
